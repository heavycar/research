# HCB research repository

This repository serves as a place to collect and discuss evidence supporting the existence of a bug
in Rocket League which can lead to inconsistent input response, "sluggishness", "heaviness", weak
shots, and various timing issues.

## What is the "heavy car" bug?

A [survey][survey] posted to both members of the "Heavy Car Enthusiasts" Discord group and
participants of a long-running [thread][thread] on the Psyonix forum, as well as general discussion
amongst those experiencing the issue produced these common descriptors:

- That the player car sometimes feels "heavy", "sluggish", "unresponsive", or "delayed".
- That the game sometimes ignores inputs, dampens them, or resists against sudden change in the
  car's rotation or speed, resulting in slower or unregistered flips, slow aerial takeoffs, slow
  turning response, a wider turning radius, and general slow acceleration.
- That ball hits are sometimes significantly weaker than expected.
- That the speed of the game feels inconsistent.
- That the car can feel "desynchronized" or "stuck in a bad state".

## When does it occur?

The consensus is less clear, but some people report:

- That the effects are worse during the day.
- That alt-tabbing out and in of the game results in a temporarily but immediate change in responsiveness.
- That toggling various graphics settings such as VSync can produce a similar response.

[survey]:
https://docs.google.com/forms/d/e/1FAIpQLSdy8LfIlv1IoxSd9T9xjyGFmBdso0f5HAaFj2G9Wo8zECaRrw/viewanalytics
[thread]: https://www.psyonix.com/forum/viewtopic.php?f=34&t=26634